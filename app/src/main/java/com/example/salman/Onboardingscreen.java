package com.example.salman;

import com.hololo.tutorial.library.TutorialActivity;
import android.Manifest;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Toast;

import com.hololo.tutorial.library.PermissionStep;
import com.hololo.tutorial.library.Step;
import com.hololo.tutorial.library.TutorialActivity;

public class Onboardingscreen extends TutorialActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addFragment(new Step.Builder().setTitle("Welcome")
                .setContent("smart mobile application")
                .setBackgroundColor(Color.parseColor("#FF0957")) // int background colo
                .setDrawable(R.drawable.ic_launcher_foreground) // int top drawable
                .setSummary("smart application")
                .build());

        addFragment(new Step.Builder().setTitle("This is header")
                .setContent("This is content")
                .setBackgroundColor(Color.parseColor("#FF0957")) // int background colo
                .setDrawable(R.drawable.award) // int top drawable
                .setSummary("This is summary")
                .build());

        addFragment(new Step.Builder().setTitle("This is header")
                .setContent("This is content")
                .setBackgroundColor(Color.parseColor("#FF0957")) // int background colo
                .setDrawable(R.drawable.ic_launcher_foreground) // int top drawable
                .setSummary("This is summary")
                .build());


    }
    @Override
    public void finishTutorial() {
        Intent i = new Intent(Onboardingscreen.this, MainActivity.class);
        startActivity(i);
    }

    @Override
    public void currentFragmentPosition(int position) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
