package com.example.salman;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.content.Intent;

import android.view.View;
import android.widget.Toast;

import java.util.regex.Pattern;

public class signupp extends AppCompatActivity {
    private static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    "(?=.*[a-zA-Z])" +
                    "(?=.*[@#$%^&+=])" +
                    "(?=\\S+$)" +
                    ".{4,}" +
                    "$");
    dbhepler db;
    EditText username,email,conpass,pass;
    Button signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signupp);

        signup = findViewById(R.id.buttonsignups);
        username = findViewById(R.id.usernames);
        email = findViewById(R.id.newemails);
        pass = findViewById(R.id.passS);
        conpass = findViewById(R.id.cons);

        validateusern();
        validatemail();
        validatepas();
        validatecofpass();
        ChangeScreen();
    }
    void ChangeScreen()
    {
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s1= email.getText().toString();
                String s2= pass.getText().toString();
                String s3= conpass.getText().toString();
                if (s1.equals("")|| s2.equals("") || s3.equals(""))
                {
                    Toast.makeText(getApplicationContext(),"fields cannot be empty",Toast.LENGTH_SHORT).show();
                }
                else {
                    if (s2.equals(s3)){
                        Boolean checkmail = db.checkmail(s1);
                        if (checkmail==true) {
                            Boolean insert = db.insert(s1, s2);

                            if (insert == true){
                                Toast.makeText(getApplicationContext(),"Registered successfully",Toast.LENGTH_SHORT).show();
                                Intent abd = new Intent (signupp.this,MainActivity.class);
                                startActivity(abd);
                            }
                        }
                        else {
                            Toast.makeText(getApplicationContext(),"already exists",Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        Toast.makeText(getApplicationContext(),"password doesnot matched",Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }
    private boolean validateusern() {
        String usernameInput = username.getEditableText().toString().trim();

        if (usernameInput.isEmpty()) {
            username.setError("Field can't be empty");
            return false;
        } else if (usernameInput.length() > 15) {
            username.setError("Username too long");
            return false;
        } else {
            username.setError(null);
            return true;
        }
    }
    private boolean validatemail() {
        String usernameInput = email.getEditableText().toString().trim();

        if (usernameInput.isEmpty()) {
            email.setError("Field can't be empty");
            return false;
        } else if (usernameInput.length() > 15) {
            email.setError("Username too long");
            return false;
        } else {
            email.setError(null);
            return true;
        }
    }
    private boolean validatepas() {
        String passwordInput = pass.getEditableText().toString().trim();

        if (passwordInput.isEmpty()) {
            pass.setError("Field can't be empty");
            return false;
        } else if (!PASSWORD_PATTERN.matcher(passwordInput).matches()) {
            pass.setError("Password too weak");
            return false;
        } else {
            pass.setError(null);
            return true;

        }
    }
    private boolean validatecofpass() {
        String passwordInput = conpass.getEditableText().toString().trim();

        if (passwordInput.isEmpty()) {
            conpass.setError("Field can't be empty");
            return false;
        } else if (!PASSWORD_PATTERN.matcher(passwordInput).matches()) {
            conpass.setError("Password too weak");
            return false;
        } else {
            conpass.setError(null);
            return true;

        }
    }
}
